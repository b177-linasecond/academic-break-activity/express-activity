const express = require("express")
	  app = express()
	  port = 4001

app.use(express.json())

app.use(express.urlencoded({extended : true}))

let mockDB = []

app.post('/signup', (req, res) => {

	if(req.body.username !== '' && req.body.password !== '' && req.body.firstName !== '' && req.body.lastName !== '') {
		mockDB.push(req.body)
		res.send(`You successfully registered!`)
	}

	else {
		res.send(`Please input all the details required!`)
	}

})

app.post('/login', (req, res) => {
	console.log(mockDB)

	for(let i = 0; i < mockDB.length; i++) {
		if(req.body.username == mockDB[i].username && req.body.password == mockDB[i].password) {
			res.send(`You successfully logged in!`)
		}
		else {
			res.send(`Please enter the correct username and password!`)
		}
	}

})

app.listen(port, () => console.log(`Server is running at port ${port}`))